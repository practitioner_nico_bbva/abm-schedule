import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --abm-schedules; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit;
  outline: none; }

select::-ms-expand {
  display: none; }

h1 {
  text-align: center;
  font-size: 21px;
  color: #616161; }

h3 {
  text-align: center;
  font-size: 16px;
  color: #6060b9; }

.block-hour {
  display: flex;
  width: 100%;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: center;
  padding: 20px 0; }
  .block-hour select {
    font-size: 50px;
    background: transparent;
    border: none;
    padding: 0 20px;
    -webkit-appearance: none;
    appearance: none; }
    .block-hour select option {
      font-size: 16px; }
  .block-hour span {
    font-size: 35px; }

.bt-save {
  text-align: center;
  margin-top: 40px; }
  .bt-save button {
    background: #004481;
    padding: 14px 30px;
    border: none;
    color: white;
    display: inline-block; }
    .bt-save button:hover {
      background: #235e92; }
`;