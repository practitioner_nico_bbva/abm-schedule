import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './abm-schedules-styles.js';
/**
This component es visual y es utilizado para seleccionar Horas y minutos.

Example:

```html
<abm-schedules></abm-schedules>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class AbmSchedules extends LitElement {
  static get is() {
    return 'abm-schedules';
  }

  // Declare properties
  static get properties() {
    return {
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('abm-schedules-shared-styles').cssText}
    `;
  }

  /**
   * Resetea los valores a 0
   */
  reset() {
    const hour = this.shadowRoot.querySelector('#hour-id');
    const minutes = this.shadowRoot.querySelector('#minutes-id');
    hour.selectedIndex = 0;
    minutes.selectedIndex = 0;
    return;
  }

  /**
   * 
   * @param {Number} maxNumber Itereacciones del arreglo.
   * Retorna un arreglo con los números formateados, ya sea para una lista de 23 o 59.
   */
  _fillArray(maxNumber) {
    let myArray = [];
    for (let index = 0; index <= maxNumber; index++) {
      myArray.push(('0' + index).slice(-2));
    }
    return myArray;
  }

  /**
   * 
   * @param {Number} maxNumber Iteracciones del arreglo.
   * @param {String} name Para identificar el componente.
   */
  _getSelect(maxNumber, name) {
    /** Llena el arreglo formateado. */
    const myArray = this._fillArray(maxNumber);

    // Re torna el componente select con todas las opciones.
    return html`
      <select name="${name}" id="${name}-id">
       ${ myArray.map((element) => {
      return html`
          <option value="${ element}"> ${element} </option>
         `
    })}
      </select>
    `;
  }

  /**
   * Este método envía un evento con la hora y los minutos seleccionados. 
   */
  _save() {
    const hour = this.shadowRoot.querySelector('#hour-id');
    const minutes = this.shadowRoot.querySelector('#minutes-id');

    const detail = {
      hour: hour.selectedIndex,
      minutes: minutes.selectedIndex
    }

    this.dispatchEvent(new CustomEvent('schedule_saved', {detail, composed: true, bubbles: true })); 
  }

  /**
   * Renderiza dos componentes SELECT para seleccionar los valores.
   * Renderiza un botón para guardar.
   */
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <h1> Selecciona la hora en que quieres saber el estado de una estación.</h1>
      <h3>Recuerda que a este horario recibirás una notificación.</h3>
      <div class="block-hour">
            ${ this._getSelect(23, 'hour')}
            <span>:</span>
            ${ this._getSelect(59, 'minutes')}
      </div>
      <div class="bt-save">
        <button @click="${ this._save}">Guardar</button>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(AbmSchedules.is, AbmSchedules);
